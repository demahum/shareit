class Post < ApplicationRecord
  validates :title, presence: true, length: { minimum: 6, maximum: 30 }
  validates :content, presence: true, length: { minimum: 10, maximum: 1000 }
end